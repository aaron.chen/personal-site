import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import useI18nConsumer from '../../Hooks/useI18nConsumer';
import useImageLoaded from '../../Hooks/useImageLoaded';

import faviconSrc from '../../Images/Favicon/android-chrome-192x192.png';

import WorkGroupPortlet from './Generic/WorkGroupPortlet';

const ICSWorkPortlet = ({ className = '', onLoad = () => {} }) => {
  const { t } = useI18nConsumer();

  const icsButtons = [
    {
      id: 'personalSite',
      src: faviconSrc,
      hoverText: t('OtherWorkPortlet.personalWebsite'),
      href: 'https://gitlab.com/aaron.chen/personal-site',
    },
  ];

  const loaded = useImageLoaded(faviconSrc);

  useEffect(() => {
    if (loaded) {
      onLoad();
    }
  }, [loaded, onLoad]);

  return (
    <WorkGroupPortlet
      className={className}
      title={t('OtherWorkPortlet.other')}
      buttons={icsButtons}
    />
  );
};

ICSWorkPortlet.propTypes = {
  className: PropTypes.string,

  onLoad: PropTypes.func,
};

export default ICSWorkPortlet;
