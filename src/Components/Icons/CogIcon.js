import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Cog } from 'styled-icons/fa-solid/Cog';

import IconButton from './Generic/IconButton';

const StyledCogIgon = styled(IconButton)`
  cursor: pointer;
`;

const CogIgon = ({ className = '', size = 30, onClick = () => {} }) => {
  const renderIcon = () => {
    return <Cog size={size} />;
  };

  return <StyledCogIgon className={className} renderIcon={renderIcon} onClick={onClick} />;
};

CogIgon.propTypes = {
  className: PropTypes.string,

  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onClick: PropTypes.func,
};

export default CogIgon;

