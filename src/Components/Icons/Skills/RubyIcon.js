import React from 'react';
import styled from 'styled-components';

const Stop = styled.stop`
  stop-color: ${props => props.stopColor};
`;

const Polygon = styled.polygon`
  fill: url(${props => props.url});
  fill-rule: evenodd;
`;

const Path = styled.path`
  fill: ${props => props.fill};
  fill-rule: evenodd;
`;

const RubyIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      viewBox="-10 -12 198.13 197.58"
      enableBackground="new 0 0 198.13 197.58"
      xmlSpace="preserve"
    >
      <defs id="defs3489">
        <linearGradient
          id="XMLID_17_-9"
          gradientUnits="userSpaceOnUse"
          x1="174.0737"
          y1="215.5488"
          x2="132.27631"
          y2="141.7533"
        >
          <Stop offset="0" stopColor="#fb7655" id="stop3272-4" />
          <Stop offset="0" stopColor="#fb7655" id="stop3274-5" />
          <Stop offset="0.41" stopColor="#e42b1e" id="stop3276-1" />
          <Stop offset="0.99" stopColor="#990000" id="stop3278-0" />
          <Stop offset="1" stopColor="#990000" id="stop3280-3" />
        </linearGradient>
        <linearGradient
          id="XMLID_18_-7"
          gradientUnits="userSpaceOnUse"
          x1="194.895"
          y1="153.5576"
          x2="141.0276"
          y2="117.4093"
        >
          <Stop offset="0" stopColor="#871101" id="stop3285-8" />
          <Stop offset="0" stopColor="#871101" id="stop3287-8" />
          <Stop offset="0.99" stopColor="#911209" id="stop3289-6" />
          <Stop offset="1" stopColor="#911209" id="stop3291-0" />
        </linearGradient>
        <linearGradient
          id="XMLID_19_-4"
          gradientUnits="userSpaceOnUse"
          x1="151.79539"
          y1="217.7852"
          x2="97.929703"
          y2="181.638"
        >
          <Stop offset="0" stopColor="#871101" id="stop3296-6" />
          <Stop offset="0" stopColor="#871101" id="stop3298-7" />
          <Stop offset="0.99" stopColor="#911209" id="stop3300-6" />
          <Stop offset="1" stopColor="#911209" id="stop3302-0" />
        </linearGradient>
        <linearGradient
          id="XMLID_20_-9"
          gradientUnits="userSpaceOnUse"
          x1="38.696301"
          y1="127.3906"
          x2="47.046902"
          y2="181.66141"
        >
          <Stop offset="0" stopColor="#ffffff" id="stop3307-7" />
          <Stop offset="0" stopColor="#ffffff" id="stop3309-5" />
          <Stop offset="0.23" stopColor="#e57252" id="stop3311-9" />
          <Stop offset="0.46" stopColor="#de3b20" id="stop3313-7" />
          <Stop offset="0.99" stopColor="#a60003" id="stop3315-8" />
          <Stop offset="1" stopColor="#a60003" id="stop3317-5" />
        </linearGradient>
        <linearGradient
          id="XMLID_21_-3"
          gradientUnits="userSpaceOnUse"
          x1="96.132797"
          y1="76.715302"
          x2="99.209602"
          y2="132.1021"
        >
          <Stop offset="0" stopColor="#ffffff" id="stop3322-3" />
          <Stop offset="0" stopColor="#ffffff" id="stop3324-8" />
          <Stop offset="0.23" stopColor="#e4714e" id="stop3326-3" />
          <Stop offset="0.56" stopColor="#be1a0d" id="stop3328-7" />
          <Stop offset="0.99" stopColor="#a80d00" id="stop3330-9" />
          <Stop offset="1" stopColor="#a80d00" id="stop3332-3" />
        </linearGradient>
        <linearGradient
          id="XMLID_22_-7"
          gradientUnits="userSpaceOnUse"
          x1="147.103"
          y1="25.521"
          x2="156.3141"
          y2="65.216202"
        >
          <Stop offset="0" stopColor="#ffffff" id="stop3337-8" />
          <Stop offset="0" stopColor="#ffffff" id="stop3339-7" />
          <Stop offset="0.18" stopColor="#e46342" id="stop3341-4" />
          <Stop offset="0.4" stopColor="#c82410" id="stop3343-1" />
          <Stop offset="0.99" stopColor="#a80d00" id="stop3345-9" />
          <Stop offset="1" stopColor="#a80d00" id="stop3347-0" />
        </linearGradient>
        <linearGradient
          id="XMLID_23_-9"
          gradientUnits="userSpaceOnUse"
          x1="118.9761"
          y1="11.5415"
          x2="158.66859"
          y2="-8.3048"
        >
          <Stop offset="0" stopColor="#ffffff" id="stop3352-8" />
          <Stop offset="0" stopColor="#ffffff" id="stop3354-8" />
          <Stop offset="0.54" stopColor="#c81f11" id="stop3356-5" />
          <Stop offset="0.99" stopColor="#bf0905" id="stop3358-8" />
          <Stop offset="1" stopColor="#bf0905" id="stop3360-4" />
        </linearGradient>
        <linearGradient
          id="XMLID_24_-3"
          gradientUnits="userSpaceOnUse"
          x1="3.9033"
          y1="113.5547"
          x2="7.1701999"
          y2="146.2628"
        >
          <Stop offset="0" stopColor="#ffffff" id="stop3365-7" />
          <Stop offset="0" stopColor="#ffffff" id="stop3367-1" />
          <Stop offset="0.31" stopColor="#de4024" id="stop3369-3" />
          <Stop offset="0.99" stopColor="#bf190b" id="stop3371-8" />
          <Stop offset="1" stopColor="#bf190b" id="stop3373-0" />
        </linearGradient>
        <linearGradient
          id="XMLID_25_-9"
          gradientUnits="userSpaceOnUse"
          x1="-18.5557"
          y1="155.10451"
          x2="135.0152"
          y2="-2.8092999"
        >
          <Stop offset="0" stopColor="#bd0012" id="stop3380-7" />
          <Stop offset="0" stopColor="#bd0012" id="stop3382-9" />
          <Stop offset="0.07" stopColor="#ffffff" id="stop3384-9" />
          <Stop offset="0.17" stopColor="#ffffff" id="stop3386-3" />
          <Stop offset="0.27" stopColor="#c82f1c" id="stop3388-2" />
          <Stop offset="0.33" stopColor="#820c01" id="stop3390-4" />
          <Stop offset="0.46" stopColor="#a31601" id="stop3392-3" />
          <Stop offset="0.72" stopColor="#b31301" id="stop3394-7" />
          <Stop offset="0.99" stopColor="#e82609" id="stop3396-1" />
          <Stop offset="1" stopColor="#e82609" id="stop3398-2" />
        </linearGradient>
        <linearGradient
          id="XMLID_26_-2"
          gradientUnits="userSpaceOnUse"
          x1="99.074699"
          y1="171.0332"
          x2="52.817699"
          y2="159.61659"
        >
          <Stop offset="0" stopColor="#8c0c01" id="stop3403-0" />
          <Stop offset="0" stopColor="#8c0c01" id="stop3405-2" />
          <Stop offset="0.54" stopColor="#990c00" id="stop3407-1" />
          <Stop offset="0.99" stopColor="#a80d0e" id="stop3409-7" />
          <Stop offset="1" stopColor="#a80d0e" id="stop3411-5" />
        </linearGradient>
        <linearGradient
          id="XMLID_27_-1"
          gradientUnits="userSpaceOnUse"
          x1="178.52589"
          y1="115.5146"
          x2="137.43269"
          y2="78.683998"
        >
          <Stop offset="0" stopColor="#7e110b" id="stop3416-7" />
          <Stop offset="0" stopColor="#7e110b" id="stop3418-4" />
          <Stop offset="0.99" stopColor="#9e0c00" id="stop3420-1" />
          <Stop offset="1" stopColor="#9e0c00" id="stop3422-7" />
        </linearGradient>
        <linearGradient
          id="XMLID_28_-1"
          gradientUnits="userSpaceOnUse"
          x1="193.6235"
          y1="47.937"
          x2="173.15421"
          y2="26.053801"
        >
          <Stop offset="0" stopColor="#79130d" id="stop3427-1" />
          <Stop offset="0" stopColor="#79130d" id="stop3429-1" />
          <Stop offset="0.99" stopColor="#9e120b" id="stop3431-7" />
          <Stop offset="1" stopColor="#9e120b" id="stop3433-0" />
        </linearGradient>
        <radialGradient
          id="XMLID_29_-4"
          cx="143.8315"
          cy="79.388199"
          r="50.357601"
          gradientUnits="userSpaceOnUse"
        >
          <Stop offset="0" stopColor="#a80d00" id="stop3440-0" />
          <Stop offset="0" stopColor="#a80d00" id="stop3442-8" />
          <Stop offset="0.99" stopColor="#7e0e08" id="stop3444-5" />
          <Stop offset="1" stopColor="#7e0e08" id="stop3446-1" />
        </radialGradient>
        <radialGradient
          id="XMLID_30_-6"
          cx="74.0923"
          cy="145.75101"
          r="66.943703"
          gradientUnits="userSpaceOnUse"
        >
          <Stop offset="0" stopColor="#a30c00" id="stop3451-6" />
          <Stop offset="0" stopColor="#a30c00" id="stop3453-2" />
          <Stop offset="0.99" stopColor="#800e08" id="stop3455-1" />
          <Stop offset="1" stopColor="#800e08" id="stop3457-9" />
        </radialGradient>
        <linearGradient
          id="XMLID_31_-6"
          gradientUnits="userSpaceOnUse"
          x1="26.669901"
          y1="197.33591"
          x2="9.9886999"
          y2="140.742"
        >
          <Stop offset="0" stopColor="#8b2114" id="stop3462-4" />
          <Stop offset="0" stopColor="#8b2114" id="stop3464-8" />
          <Stop offset="0.43" stopColor="#9e100a" id="stop3466-0" />
          <Stop offset="0.99" stopColor="#b3100c" id="stop3468-8" />
          <Stop offset="1" stopColor="#b3100c" id="stop3470-1" />
        </linearGradient>
        <linearGradient
          id="XMLID_32_-0"
          gradientUnits="userSpaceOnUse"
          x1="154.6411"
          y1="9.7979002"
          x2="192.039"
          y2="26.305901"
        >
          <Stop offset="0" stopColor="#b31000" id="stop3475-2" />
          <Stop offset="0" stopColor="#b31000" id="stop3477-2" />
          <Stop offset="0.44" stopColor="#910f08" id="stop3479-9" />
          <Stop offset="0.99" stopColor="#791c12" id="stop3481-7" />
          <Stop offset="1" stopColor="#791c12" id="stop3483-5" />
        </linearGradient>
        <linearGradient
          y2="141.7533"
          x2="132.27631"
          y1="215.5488"
          x1="174.0737"
          gradientUnits="userSpaceOnUse"
          id="linearGradient3448"
          xlinkHref="#XMLID_17_-9"
        />
      </defs>
      <linearGradient
        id="XMLID_17_"
        gradientUnits="userSpaceOnUse"
        x1="174.0737"
        y1="215.5488"
        x2="132.2763"
        y2="141.7533"
      >
        <Stop offset="0" stopColor="#fb7655" id="stop3272" />
        <Stop offset="0" stopColor="#fb7655" id="stop3274" />
        <Stop offset="0.41" stopColor="#e42b1e" id="stop3276" />
        <Stop offset="0.99" stopColor="#990000" id="stop3278" />
        <Stop offset="1" stopColor="#990000" id="stop3280" />
      </linearGradient>
      <linearGradient
        id="XMLID_18_"
        gradientUnits="userSpaceOnUse"
        x1="194.895"
        y1="153.5576"
        x2="141.0276"
        y2="117.4093"
      >
        <Stop offset="0" stopColor="#871101" id="stop3285" />
        <Stop offset="0" stopColor="#871101" id="stop3287" />
        <Stop offset="0.99" stopColor="#911209" id="stop3289" />
        <Stop offset="1" stopColor="#911209" id="stop3291" />
      </linearGradient>
      <linearGradient
        id="XMLID_19_"
        gradientUnits="userSpaceOnUse"
        x1="151.7954"
        y1="217.7852"
        x2="97.9297"
        y2="181.638"
      >
        <Stop offset="0" stopColor="#871101" id="stop3296" />
        <Stop offset="0" stopColor="#871101" id="stop3298" />
        <Stop offset="0.99" stopColor="#911209" id="stop3300" />
        <Stop offset="1" stopColor="#911209" id="stop3302" />
      </linearGradient>
      <linearGradient
        id="XMLID_20_"
        gradientUnits="userSpaceOnUse"
        x1="38.6963"
        y1="127.3906"
        x2="47.0469"
        y2="181.6614"
      >
        <Stop offset="0" stopColor="#ffffff" id="stop3307" />
        <Stop offset="0" stopColor="#ffffff" id="stop3309" />
        <Stop offset="0.23" stopColor="#e57252" id="stop3311" />
        <Stop offset="0.46" stopColor="#de3b20" id="stop3313" />
        <Stop offset="0.99" stopColor="#a60003" id="stop3315" />
        <Stop offset="1" stopColor="#a60003" id="stop3317" />
      </linearGradient>
      <linearGradient
        id="XMLID_21_"
        gradientUnits="userSpaceOnUse"
        x1="96.1328"
        y1="76.7153"
        x2="99.2096"
        y2="132.1021"
      >
        <Stop offset="0" stopColor="#ffffff" id="stop3322" />
        <Stop offset="0" stopColor="#ffffff" id="stop3324" />
        <Stop offset="0.23" stopColor="#e4714e" id="stop3326" />
        <Stop offset="0.56" stopColor="#be1a0d" id="stop3328" />
        <Stop offset="0.99" stopColor="#a80d00" id="stop3330" />
        <Stop offset="1" stopColor="#a80d00" id="stop3332" />
      </linearGradient>
      <linearGradient
        id="XMLID_22_"
        gradientUnits="userSpaceOnUse"
        x1="147.103"
        y1="25.521"
        x2="156.3141"
        y2="65.2162"
      >
        <Stop offset="0" stopColor="#ffffff" id="stop3337" />
        <Stop offset="0" stopColor="#ffffff" id="stop3339" />
        <Stop offset="0.18" stopColor="#e46342" id="stop3341" />
        <Stop offset="0.4" stopColor="#c82410" id="stop3343" />
        <Stop offset="0.99" stopColor="#a80d00" id="stop3345" />
        <Stop offset="1" stopColor="#a80d00" id="stop3347" />
      </linearGradient>
      <linearGradient
        id="XMLID_23_"
        gradientUnits="userSpaceOnUse"
        x1="118.9761"
        y1="11.5415"
        x2="158.6686"
        y2="-8.3048"
      >
        <Stop offset="0" stopColor="#ffffff" id="stop3352" />
        <Stop offset="0" stopColor="#ffffff" id="stop3354" />
        <Stop offset="0.54" stopColor="#c81f11" id="stop3356" />
        <Stop offset="0.99" stopColor="#bf0905" id="stop3358" />
        <Stop offset="1" stopColor="#bf0905" id="stop3360" />
      </linearGradient>
      <linearGradient
        id="XMLID_24_"
        gradientUnits="userSpaceOnUse"
        x1="3.9033"
        y1="113.5547"
        x2="7.1702"
        y2="146.2628"
      >
        <Stop offset="0" stopColor="#ffffff" id="stop3365" />
        <Stop offset="0" stopColor="#ffffff" id="stop3367" />
        <Stop offset="0.31" stopColor="#de4024" id="stop3369" />
        <Stop offset="0.99" stopColor="#bf190b" id="stop3371" />
        <Stop offset="1" stopColor="#bf190b" id="stop3373" />
      </linearGradient>
      <linearGradient
        id="XMLID_25_"
        gradientUnits="userSpaceOnUse"
        x1="-18.5557"
        y1="155.1045"
        x2="135.0152"
        y2="-2.8093"
      >
        <Stop offset="0" stopColor="#bd0012" id="stop3380" />
        <Stop offset="0" stopColor="#bd0012" id="stop3382" />
        <Stop offset="0.07" stopColor="#ffffff" id="stop3384" />
        <Stop offset="0.17" stopColor="#ffffff" id="stop3386" />
        <Stop offset="0.27" stopColor="#c82f1c" id="stop3388" />
        <Stop offset="0.33" stopColor="#820c01" id="stop3390" />
        <Stop offset="0.46" stopColor="#a31601" id="stop3392" />
        <Stop offset="0.72" stopColor="#b31301" id="stop3394" />
        <Stop offset="0.99" stopColor="#e82609" id="stop3396" />
        <Stop offset="1" stopColor="#e82609" id="stop3398" />
      </linearGradient>
      <linearGradient
        id="XMLID_26_"
        gradientUnits="userSpaceOnUse"
        x1="99.0747"
        y1="171.0332"
        x2="52.8177"
        y2="159.6166"
      >
        <Stop offset="0" stopColor="#8c0c01" id="stop3403" />
        <Stop offset="0" stopColor="#8c0c01" id="stop3405" />
        <Stop offset="0.54" stopColor="#990c00" id="stop3407" />
        <Stop offset="0.99" stopColor="#a80d0e" id="stop3409" />
        <Stop offset="1" stopColor="#a80d0e" id="stop3411" />
      </linearGradient>
      <linearGradient
        id="XMLID_27_"
        gradientUnits="userSpaceOnUse"
        x1="178.5259"
        y1="115.5146"
        x2="137.4327"
        y2="78.684"
      >
        <Stop offset="0" stopColor="#7e110b" id="stop3416" />
        <Stop offset="0" stopColor="#7e110b" id="stop3418" />
        <Stop offset="0.99" stopColor="#9e0c00" id="stop3420" />
        <Stop offset="1" stopColor="#9e0c00" id="stop3422" />
      </linearGradient>
      <linearGradient
        id="XMLID_28_"
        gradientUnits="userSpaceOnUse"
        x1="193.6235"
        y1="47.937"
        x2="173.1542"
        y2="26.0538"
      >
        <Stop offset="0" stopColor="#79130d" id="stop3427" />
        <Stop offset="0" stopColor="#79130d" id="stop3429" />
        <Stop offset="0.99" stopColor="#9e120b" id="stop3431" />
        <Stop offset="1" stopColor="#9e120b" id="stop3433" />
      </linearGradient>
      <radialGradient
        id="XMLID_29_"
        cx="143.8315"
        cy="79.3882"
        r="50.3576"
        gradientUnits="userSpaceOnUse"
      >
        <Stop offset="0" stopColor="#a80d00" id="stop3440" />
        <Stop offset="0" stopColor="#a80d00" id="stop3442" />
        <Stop offset="0.99" stopColor="#7e0e08" id="stop3444" />
        <Stop offset="1" stopColor="#7e0e08" id="stop3446" />
      </radialGradient>
      <radialGradient
        id="XMLID_30_"
        cx="74.0923"
        cy="145.751"
        r="66.9437"
        gradientUnits="userSpaceOnUse"
      >
        <Stop offset="0" stopColor="#a30c00" id="stop3451" />
        <Stop offset="0" stopColor="#a30c00" id="stop3453" />
        <Stop offset="0.99" stopColor="#800e08" id="stop3455" />
        <Stop offset="1" stopColor="#800e08" id="stop3457" />
      </radialGradient>
      <linearGradient
        id="XMLID_31_"
        gradientUnits="userSpaceOnUse"
        x1="26.6699"
        y1="197.3359"
        x2="9.9887"
        y2="140.742"
      >
        <Stop offset="0" stopColor="#8b2114" id="stop3462" />
        <Stop offset="0" stopColor="#8b2114" id="stop3464" />
        <Stop offset="0.43" stopColor="#9e100a" id="stop3466" />
        <Stop offset="0.99" stopColor="#b3100c" id="stop3468" />
        <Stop offset="1" stopColor="#b3100c" id="stop3470" />
      </linearGradient>
      <linearGradient
        id="XMLID_32_"
        gradientUnits="userSpaceOnUse"
        x1="154.6411"
        y1="9.7979"
        x2="192.039"
        y2="26.3059"
      >
        <Stop offset="0" stopColor="#b31000" id="stop3475" />
        <Stop offset="0" stopColor="#b31000" id="stop3477" />
        <Stop offset="0.44" stopColor="#910f08" id="stop3479" />
        <Stop offset="0.99" stopColor="#791c12" id="stop3481" />
        <Stop offset="1" stopColor="#791c12" id="stop3483" />
      </linearGradient>
      <g transform="scale(0.9)">
        <Polygon
          url="#linearGradient3448"
          clipRule="evenodd"
          points="153.5,130.41 40.38,197.58 186.849,187.641 198.13,39.95 "
          id="polygon3282"
        />
        <Polygon
          url="#XMLID_18_-7"
          clipRule="evenodd"
          points="187.089,187.54 174.5,100.65 140.209,145.93 "
          id="polygon3293"
        />
        <Polygon
          url="#XMLID_19_-4"
          clipRule="evenodd"
          points="187.259,187.54 95.03,180.3 40.87,197.391 "
          id="polygon3304"
        />
        <Polygon
          url="#XMLID_20_-9"
          clipRule="evenodd"
          points="41,197.41 64.04,121.93 13.34,132.771 "
          id="polygon3319"
        />
        <Polygon
          url="#XMLID_21_-3"
          clipRule="evenodd"
          points="140.2,146.18 119,63.14 58.33,120.01 "
          id="polygon3334"
        />
        <Polygon
          url="#XMLID_22_-7"
          clipRule="evenodd"
          points="193.32,64.31 135.97,17.47 120,69.1 "
          id="polygon3349"
        />
        <Polygon
          url="#XMLID_23_-9"
          clipRule="evenodd"
          points="166.5,0.77 132.77,19.41 111.49,0.52 "
          id="polygon3362"
        />
        <Polygon
          url="#XMLID_24_-3"
          clipRule="evenodd"
          points="0,158.09 14.13,132.32 2.7,101.62 "
          id="polygon3375"
        />
        <Path
          fill="#ffffff"
          clipRule="evenodd"
          d="m 1.94,100.65 11.5,32.62 49.97,-11.211 57.05,-53.02 L 136.56,17.9 111.209,0 68.109,16.13 C 54.53,28.76 28.18,53.75 27.23,54.22 26.29,54.7 9.83,85.81 1.94,100.65 z"
          id="path3377"
        />
        <Path
          fill="url(#XMLID_25_-9)"
          clipRule="evenodd"
          d="m 42.32,42.05 c 29.43,-29.18 67.37,-46.42 81.93,-31.73 14.551,14.69 -0.88,50.39 -30.31,79.56 -29.43,29.17 -66.9,47.36 -81.45,32.67 -14.56,-14.68 0.4,-51.33 29.83,-80.5 z"
          id="path3400"
        />
        <Path
          fill="url(#XMLID_26_-2)"
          clipRule="evenodd"
          d="m 41,197.38 22.86,-75.72 75.92,24.39 C 112.33,171.79 81.8,193.55 41,197.38 z"
          id="path3413"
        />
        <Path
          fill="url(#XMLID_27_-1)"
          clipRule="evenodd"
          d="m 120.56,68.89 19.49,77.2 C 162.98,121.98 183.56,96.06 193.639,64 l -73.079,4.89 z"
          id="path3424"
        />
        <Path
          fill="url(#XMLID_28_-1)"
          clipRule="evenodd"
          d="m 193.44,64.39 c 7.8,-23.54 9.6,-57.31 -27.181,-63.58 l -30.18,16.67 57.361,46.91 z"
          id="path3435"
        />
        <Path
          fill="#9e1209"
          clipRule="evenodd"
          d="m 0,157.75 c 1.08,38.851 29.11,39.43 41.05,39.771 L 13.47,133.11 0,157.75 z"
          id="path3437"
        />
        <Path
          fill="url(#XMLID_29_-4)"
          clipRule="evenodd"
          d="m 120.669,69.01 c 17.62,10.83 53.131,32.58 53.851,32.98 1.119,0.63 15.31,-23.93 18.53,-37.81 l -72.381,4.83 z"
          id="path3448"
        />
        <Path
          fill="url(#XMLID_30_-6)"
          clipRule="evenodd"
          d="m 63.83,121.66 30.56,58.96 c 18.07,-9.8 32.22,-21.74 45.18,-34.53 L 63.83,121.66 z"
          id="path3459"
        />
        <Path
          fill="url(#XMLID_31_-6)"
          clipRule="evenodd"
          d="m 13.35,133.19 -4.33,51.56 c 8.17,11.16 19.41,12.13 31.2,11.26 -8.53,-21.23 -25.57,-63.68 -26.87,-62.82 z"
          id="path3472"
        />
        <Path
          fill="url(#XMLID_32_-0)"
          clipRule="evenodd"
          d="m 135.9,17.61 60.71,8.52 C 193.37,12.4 183.42,3.54 166.46,0.77 L 135.9,17.61 z"
          id="path3485"
        />
      </g>
    </svg>
  );
};

export default RubyIcon;
