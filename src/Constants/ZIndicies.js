export const Z_SPACE_BACKGROUND = -100;
export const Z_SPACE_FADER = -99;

export const Z_PAGE_SHADOW = -50;

export const Z_SIDEBAR_OVERLAY = 499;
export const Z_SIDEBAR = 500;

export const Z_DEFAULT_OVERLAY = 999;
