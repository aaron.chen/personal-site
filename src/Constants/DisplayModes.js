import { createContext } from 'react';

export const MOBILE = Symbol('Mobile Device');
export const TABLET = Symbol('Tablet Device');
export const DESKTOP = Symbol('Desktop Device');

export const DisplayModeContext = createContext(DESKTOP);
