export const EASE_IN = 'cubic-bezier(0.23, 1, 0.32, 1)';

export const PALETTE_EASE_IN = `color 1250ms ${EASE_IN} 0ms, background-color 1250ms ${EASE_IN} 0ms, border-color 1250ms ${EASE_IN} 0ms, opacity 1250ms ${EASE_IN} 0ms`;
export const HOVER_COLOR_EASE_IN = `color 450ms ${EASE_IN} 0ms, background-color 450ms ${EASE_IN} 0ms, opacity 450ms ${EASE_IN} 0ms, box-shadow 450ms ${EASE_IN} 0ms, border-color 450ms ${EASE_IN} 0ms`;
export const OVERLAY_EASE_IN = `opacity 450ms ${EASE_IN} 0ms, background 450ms ${EASE_IN} 0ms`;
export const HOVER_SIZE_EASE_IN = `font-size 450ms ${EASE_IN} 0ms, height 450ms ${EASE_IN} 0ms, width 450ms ${EASE_IN} 0ms`;

export const LINEAR = 'linear';
